import XCTest

import MyFinalSDKPackageTests

var tests = [XCTestCaseEntry]()
tests += MyFinalSDKPackageTests.allTests()
XCTMain(tests)
